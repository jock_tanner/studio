import os
from django.conf import ImproperlyConfigured

try:
    from .local import *
except ImportError:
    raise ImproperlyConfigured('''

    Please create local settings file using the “local.py.template” file
    provided in the config templates folder.

    ''')
